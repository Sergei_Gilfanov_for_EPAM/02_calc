package calc;

public class ParseErrorException extends Exception {
  ParseErrorException(String s) {
    super(s);
  }

  ParseErrorException(String s, Throwable t) {
    super(s, t);
  }
}
