package calc;

import java.util.List;
import java.io.IOException;
import java.util.ArrayList;

/*
 * Абстракция для представления выражения из операторов умножения и деления
 */
public class Factor {
  Term head;
  List<FactorTailItem> tail;

  private Factor(Term aHead, List<FactorTailItem> aTail) {
    head = aHead;
    tail = aTail;
  }

  public CalcValue getValue() {
    CalcValue value = head.getValue();
    for (FactorTailItem tailItem : tail) {
      value = tailItem.applyTo(value);
    }
    return value;
  }

  static Factor parse(CalcReader reader)
      throws IOException, ParseErrorException, TokenNotFoundException {
    // Это может вызвать, TokenNotFoundException, что означает, что и последовательности множителей
    // factor нет,
    Term head = Term.parseTerm(reader);

    List<FactorTailItem> tail = new ArrayList<FactorTailItem>();

    boolean done = false;
    do {
      try {
        FactorTailItem tailItem = FactorTailItem.parse(reader);
        tail.add(tailItem);
      } catch (TokenNotFoundException exp) {
        done = true;
      }
    } while (!done);
    return new Factor(head, tail);
  }

}
