package calc;

import java.io.IOException;

public class NumberTerm extends Term {
  private CalcValue value;

  public NumberTerm(CalcValue aValue) {
    value = aValue;
  }

  CalcValue getValue() {
    return value;
  }

  static NumberTerm parseNumber(CalcReader reader)
      throws IOException, ParseErrorException, TokenNotFoundException {
    CalcValue value = CalcValue.parse(reader);
    return new NumberTerm(value);
  }

}
