package calc;

public class SubtractionTailItem extends ExprTailItem {

  SubtractionTailItem(Factor aFactor) {
    super(aFactor);
  }

  public CalcValue applyTo(CalcValue head) {
    return head.subtract(this.factor.getValue());
  }
}
