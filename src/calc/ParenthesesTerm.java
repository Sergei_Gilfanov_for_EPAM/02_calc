package calc;

import java.io.IOException;

public class ParenthesesTerm extends Term {
  Expr expr;

  private ParenthesesTerm(Expr aExpr) {
    expr = aExpr;
  }

  CalcValue getValue() {
    return expr.getValue();
  }

  static public Term parseParenthesesTerm(CalcReader reader)
      throws IOException, ParseErrorException, TokenNotFoundException {
    int c = reader.skipWhitespacesRead();
    if (c != '(') {
      reader.unread(c);
      throw new TokenNotFoundException();
    }
    Expr expr;

    try {
      expr = Expr.parse(reader);
    } catch (TokenNotFoundException exp) {
      throw new ParseErrorException("Ожидается непустое выражение");
    }

    c = reader.skipWhitespacesRead();
    if (c != ')') {
      throw new ParseErrorException("Ожидается ')'");
    }
    return new ParenthesesTerm(expr);
  }
}
