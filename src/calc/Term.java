package calc;

import java.io.IOException;

/*
 * Абстракция для представления операндов операторов
 * 
 */
public abstract class Term {
  abstract CalcValue getValue();

  static public Term parseTerm(CalcReader reader)
      throws IOException, ParseErrorException, TokenNotFoundException {
    int c = reader.skipWhitespacesRead();
    reader.unread(c);
    if (c == '(') {
      return ParenthesesTerm.parseParenthesesTerm(reader);
    }
    return NumberTerm.parseNumber(reader);
  }
}
