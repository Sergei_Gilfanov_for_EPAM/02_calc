package calc;

public class MultiplicationTailItem extends FactorTailItem {

  MultiplicationTailItem(Term aTerm) {
    super(aTerm);
  }

  public CalcValue applyTo(CalcValue head) {
    return head.multiply(this.term.getValue());
  }
}
