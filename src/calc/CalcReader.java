package calc;

import java.io.IOException;
import java.io.PushbackReader;
import java.io.StringReader;
import java.util.ArrayDeque;
import java.util.Deque;

public class CalcReader {
  PushbackReader reader;
  static int CONTEXT_LENGT = 15;
  Deque<Character> errorContext;



  CalcReader(String s) {
    errorContext = new ArrayDeque<Character>(CONTEXT_LENGT);
    StringReader stringReader = new StringReader(s);
    reader = new PushbackReader(stringReader, 2);
  }

  private void addToContext(int c) {
    if (errorContext.size() >= CONTEXT_LENGT) {
      errorContext.remove();
    }
    errorContext.offer((char) c);
  }

  private void removeFromContext() {
    if (errorContext.size() > 0) {
      errorContext.removeLast();
    }
  }

  String getContext() {
    Character[] a = errorContext.toArray(new Character[0]);
    // FIXME : а более простого способа, чем ручная конвертация, нет?
    StringBuilder resBuilder = new StringBuilder(CONTEXT_LENGT);
    for (Character c : a) {
      resBuilder.append(c.charValue());
    }
    String retval = resBuilder.toString();
    return retval;
  }

  public int read() throws IOException {
    int c;
    c = reader.read();
    if (c > 0) {
      addToContext((char) c);
    }
    return c;
  }

  public int skipWhitespacesRead() throws IOException {
    int c;
    while (Character.isWhitespace(c = read()));
    return c;
  }


  public void unread(int c) throws IOException {
    // PushbackReader не любит, когда ему возвращают конец файла
    if (c < 0) {
      return;
    }
    removeFromContext();
    reader.unread(c);
  }

  public int peek() throws IOException {
    int retval = reader.read();
    reader.unread(retval);
    return retval;
  }
}
