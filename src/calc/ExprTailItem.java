package calc;

import java.io.IOException;

/*
 * Абстракция для представления элементов 'хвоста' выражения из операторов сложения в вычитания для
 * 1 + 2 - 3 + 4 будут созданы элементы "+2", "-3", "+4",
 */
public abstract class ExprTailItem extends OperatorTailItem {
  Factor factor;

  protected ExprTailItem(Factor aFactor) {
    factor = aFactor;
  }

  static ExprTailItem parse(CalcReader reader)
      throws IOException, ParseErrorException, TokenNotFoundException {
    int c;
    while (Character.isWhitespace(c = reader.read()));

    Factor factor;
    switch (c) {
      case '+':
      case '-':
        try {
          factor = Factor.parse(reader);
        } catch (TokenNotFoundException exp) {
          String errorMessage = "Ожидается слагаемое";
          throw new ParseErrorException(errorMessage, exp);
        }
        break;
      default:
        reader.unread(c);
        throw new TokenNotFoundException();
    }

    ExprTailItem retval;
    switch (c) {
      case '+':
        retval = new AdditionTailItem(factor);
        break;
      case '-':
        retval = new SubtractionTailItem(factor);
        break;
      default:
        // Этот случай уже должен быть отловлен предыдущем switch
        throw new RuntimeException("Unknown operator in Expr");
    }
    return retval;
  }
}
