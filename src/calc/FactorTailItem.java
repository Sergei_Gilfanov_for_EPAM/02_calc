package calc;

import java.io.IOException;

/*
 * Абстракция для представления элементов 'хвоста' выражения из операторов умножения и деления
 * Например, 2*3/4 будут созданы элементы "*3", "/4" Как представляются сами выражения и их 'головы'
 * - смотри класс Factor
 */
public abstract class FactorTailItem extends OperatorTailItem {
  Term term;

  protected FactorTailItem(Term aTerm) {
    term = aTerm;
  }

  static FactorTailItem parse(CalcReader reader)
      throws IOException, ParseErrorException, TokenNotFoundException {
    int c;
    while (Character.isWhitespace(c = reader.read()));

    Term term;
    switch (c) {
      case '*':
      case '/':
        try {
          term = Term.parseTerm(reader);
        } catch (TokenNotFoundException exp) {
          String errorMessage = "Ожидается множитель или делитель";
          throw new ParseErrorException(errorMessage, exp);
        }
        break;
      default:
        reader.unread(c);
        throw new TokenNotFoundException();
    }

    FactorTailItem retval;
    switch (c) {
      case '*':
        retval = new MultiplicationTailItem(term);
        break;
      case '/':
        retval = new DivisionTailItem(term);
        break;
      default:
        // Этот случай уже должен быть отловлен предыдущем switch
        throw new RuntimeException("Unknown operator in Factor");
    }
    return retval;
  }
}
