package calc;

import java.io.IOException;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class CalcReaderTest {

  @Before
  public void setUp() throws Exception {}

  @Test
  public void testCalcReader() throws IOException {
    CalcReader reader = new CalcReader("asdf");
    reader.read();
  }

  @Test
  public void testRead() throws IOException {
    CalcReader reader = new CalcReader("as");
    int c = reader.read();
    assertEquals("Wrong first read", c, 'a');
  }

  @Test
  public void testWhitespaceRead() throws IOException {
    CalcReader reader = new CalcReader(" as");
    int c = reader.read();
    assertEquals("Whitespace skipping error", c, ' ');
  }
  
  @Test
  public void testUnread() throws IOException {
    CalcReader reader = new CalcReader("as");
    int c = reader.read();
    reader.unread('b');
    c = reader.read();
    assertEquals("Wrong unread", c, 'b');
  }

  @Test
  public void testWhitespaceUnread() throws IOException {
    CalcReader reader = new CalcReader(" as");
    int c = reader.read();
    reader.unread(' ');
    c = reader.read();
    assertEquals("Whitespace unreading error", c, ' ');
  }
  
  @Test
  public void testPeek() throws IOException {
    CalcReader reader = new CalcReader("as");
    int c = reader.read();
    c = reader.peek();
    assertEquals("Wrong peek", c, 's');
  }

  @Test
  public void testContextRead() throws IOException {
    CalcReader reader = new CalcReader("123456789abcdefgh");
    reader.read();
    reader.read();
    String context = reader.getContext();
    assertEquals("Wrong error context", context, "12");
  }
  
  @Test
  public void testContextUnread() throws IOException {
    CalcReader reader = new CalcReader("123456789abcdefgh");
    reader.read();
    reader.read();
    reader.unread('3');
    String context = reader.getContext();
    assertEquals("Wrong error context", context, "1");
  }
  
  @Test
  public void testContextUnreadRead() throws IOException {
    CalcReader reader = new CalcReader("123456789abcdefgh");
    reader.read();
    reader.read();
    reader.unread('3');
    reader.read();
    String context = reader.getContext();
    assertEquals("Wrong error context", context, "13");
  }
}
