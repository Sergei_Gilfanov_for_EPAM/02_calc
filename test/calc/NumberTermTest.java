package calc;

import java.io.IOException;
import static org.junit.Assert.*;

import org.junit.Test;

public class NumberTermTest {
  /**
   * Тестирует значение и остаток данных в reader
   * 
   * @param value тестируемое значение
   * @param requiredValue с чем тестируем
   * @param CalcReader reader источник данных, с которого проводили парсинг
   * @param String requiredReader чему должен быть равен остаток данных в источнике
   */
  boolean valueAndRest(CalcValue value, double requiredValue, CalcReader reader,
      String requiredReader) throws IOException {
    StringBuilder dataInReader = new StringBuilder();
    int c;
    while ((c = reader.read()) > 0) {
      dataInReader.append((char) c);
    }
    return (value.equals(requiredValue)) && dataInReader.toString().equals(requiredReader);
  }
  
  @Test
  public void testGetValue() {
    CalcValue value = new CalcValue(-12e5);
    NumberTerm t = new NumberTerm(value);
    assertTrue("Value getter", t.getValue().equals(value));
  }

  @Test
  public void testNumberTermCalcReader() throws IOException,ParseErrorException,TokenNotFoundException {
    String valueAsString = "-12e5";
    double requiredValue = Double.parseDouble(valueAsString);

    CalcReader reader = new CalcReader(valueAsString);
    NumberTerm term = NumberTerm.parseNumber(reader);
    CalcValue value = term.getValue();
    String assertMessage = String.format("Parsing %s", valueAsString);
    assertTrue(assertMessage, valueAndRest(value, requiredValue, reader, ""));
  }
  
  @Test
  public void testNumberTermAddInsideToken() throws IOException,ParseErrorException,TokenNotFoundException {
    // должно парситься только -3, до +4
    String valueAsString = "-3+4";
    double requiredValue = Double.parseDouble("-3");
    
    CalcReader reader = new CalcReader(valueAsString);
    NumberTerm term = NumberTerm.parseNumber(reader);
    CalcValue value = term.getValue();
    String assertMessage = String.format("Parsing %s", valueAsString);
    assertTrue(assertMessage, valueAndRest(value, requiredValue, reader, "+4"));

  }
  
  @Test
  public void testNumberTermCalcReaderMultipleDots() throws IOException,ParseErrorException,TokenNotFoundException {
    // должно парсится только -1e2, до .e5
    String valueAsString = "-1.2.e5";
    double requiredValue = Double.parseDouble("-1.2");
    
    CalcReader reader = new CalcReader(valueAsString);
    NumberTerm term = NumberTerm.parseNumber(reader);
    CalcValue value = term.getValue();
    String assertMessage = String.format("Parsing %s", valueAsString);
    assertTrue(assertMessage, valueAndRest(value, requiredValue, reader, ".e5"));
  }
  
  @Test
  public void testNumberTermCalcReaderMultipleE() throws IOException,ParseErrorException,TokenNotFoundException {
    // должно парсится только -1e2, до e5
    String valueAsString = "-1e2e5";
    double requiredValue = Double.parseDouble("-1e2");
    
    CalcReader reader = new CalcReader(valueAsString);
    NumberTerm term = NumberTerm.parseNumber(reader);
    CalcValue value = term.getValue();
    String assertMessage = String.format("Parsing %s", valueAsString);
    assertTrue(assertMessage, valueAndRest(value, requiredValue, reader, "e5"));
  }
  
  @Test(expected=ParseErrorException.class)
  public void testNumberTermCalcReaderParseErrorNoExponent() throws IOException,ParseErrorException,TokenNotFoundException {
    String valueAsString = "+12e";
    CalcReader reader = new CalcReader(valueAsString);
    NumberTerm term = NumberTerm.parseNumber(reader); // execution will not pass this line
    CalcValue value = term.getValue();
    fail(String.format("Parse error not catched got %f", value));
  }
  
  @Test
  public void testNumberTermCalcReaderParseTillWrongCharacter() throws IOException,ParseErrorException,TokenNotFoundException {
    // Должно парсится только +12e5, до ' '
    String valueAsString = "+12e5 ";
    double requiredValue = Double.parseDouble("+12e5");
    
    CalcReader reader = new CalcReader(valueAsString);
    NumberTerm term = NumberTerm.parseNumber(reader);
    CalcValue value = term.getValue();
    String assertMessage = String.format("Parsing %s", valueAsString);
    assertTrue(assertMessage, valueAndRest(value, requiredValue, reader, " "));

  }
  
  @Test(expected=TokenNotFoundException.class)
  public void testNumberTermCalcReaderParseNoNumber() throws IOException,ParseErrorException,TokenNotFoundException {
    String valueAsString = " ";
    CalcReader reader = new CalcReader(valueAsString);
    NumberTerm term = NumberTerm.parseNumber(reader); // execution will not pass this line
    CalcValue value = term.getValue();
    fail(String.format("TokenNotFound error not catched got %f", value));
  }

}
