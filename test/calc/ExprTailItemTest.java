package calc;

import static org.junit.Assert.*;
import java.io.IOException;
import org.junit.Test;

public class ExprTailItemTest {

  @Test
  public void testParseExprTailItemAddType() throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = "+ 3*4";
    CalcReader reader = new CalcReader(valueAsString);
    ExprTailItem item = ExprTailItem.parse(reader);
    assertTrue("'+...' should give AdditionTailItem", item instanceof AdditionTailItem);
  }

  @Test
  public void testParseExprTailItemMultApply() throws IOException, ParseErrorException, TokenNotFoundException {
    CalcValue head = new CalcValue(2);
    
    String valueAsString = "+ 3*4";
    CalcReader reader = new CalcReader(valueAsString);
    ExprTailItem item = ExprTailItem.parse(reader);
    head = item.applyTo(head);
    assertTrue("AdditionTailItem: wrong calculations", head.equals(14));
  }
  
  @Test
  public void testParseExprTailItemSubtractionType() throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = "- 3*4";
    CalcReader reader = new CalcReader(valueAsString);
    ExprTailItem item = ExprTailItem.parse(reader);
    assertTrue("'+...' should give SubtractionTailItem", item instanceof SubtractionTailItem);
  }

  @Test
  public void testParseExprTailItemSubtractApply() throws IOException, ParseErrorException, TokenNotFoundException {
    CalcValue head = new CalcValue(2);
    
    String valueAsString = "- 3*4";
    CalcReader reader = new CalcReader(valueAsString);
    ExprTailItem item = ExprTailItem.parse(reader);
    head = item.applyTo(head);
    assertTrue("SubtractionTailItem: wrong calculations", head.equals(-10));
  }
  
  @Test(expected=ParseErrorException.class)
  public void testParseExprTailItemNoFactor() throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = "- q";
    CalcReader reader = new CalcReader(valueAsString);
    ExprTailItem.parse(reader); // execution will not pass this line
  }
  
  @Test(expected=TokenNotFoundException.class)
  public void testParseExprTailItemNoExprTail() throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = " ";
    CalcReader reader = new CalcReader(valueAsString);
    ExprTailItem.parse(reader); // execution will not pass this line
  }
}
