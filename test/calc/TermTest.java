package calc;

import static org.junit.Assert.*;
import java.io.IOException;
import org.junit.Test;

public class TermTest {

  @Test
  public void testParseTermNumber() throws IOException, ParseErrorException,TokenNotFoundException{
    String valueAsString = "-12e5";
    CalcReader reader = new CalcReader(valueAsString);
    Term term = Term.parseTerm(reader);
    assertTrue("Parsing", term instanceof NumberTerm);
  }
  
  @Test
  public void testParseTermNumberValue() throws IOException, ParseErrorException,TokenNotFoundException{
    String valueAsString = "-12e5";
    CalcReader reader = new CalcReader(valueAsString);
    Term term = Term.parseTerm(reader);
    assertTrue("Parsing", term.getValue().equals(-12e5));
  }
  
  @Test
  public void testParseTermParentheses() throws IOException, ParseErrorException,TokenNotFoundException{
    String valueAsString = "(1)";
    CalcReader reader = new CalcReader(valueAsString);
    Term term = Term.parseTerm(reader);
    assertTrue("Parsing", term instanceof ParenthesesTerm);
  }
  
  @Test
  public void testParseTermParenthesesValue() throws IOException, ParseErrorException,TokenNotFoundException{
    String valueAsString = "(1 + 2*3)";
    CalcReader reader = new CalcReader(valueAsString);
    Term term = Term.parseTerm(reader);
    assertTrue("Parsing", term.getValue().equals(7));
  }
  
  @Test(expected=TokenNotFoundException.class)
  public void testParseTermNoTerm() throws IOException, ParseErrorException,TokenNotFoundException{
    String valueAsString = " ";
    CalcReader reader = new CalcReader(valueAsString);
    Term term = Term.parseTerm(reader); // execution will not pass this line
    CalcValue value = term.getValue();
    fail(String.format("TokenNotFound error not catched, got %f", value));
  }

}
