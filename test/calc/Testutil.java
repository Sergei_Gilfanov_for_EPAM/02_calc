package calc;

import java.io.IOException;

public class Testutil {
  /**
   * Тестирует значение и остаток данных в reader
   * 
   * @param value тестируемое значение
   * @param requiredValue с чем тестируем
   * @param CalcReader reader источник данных, с которого проводили парсинг
   * @param String requiredReader чему должен быть равен остаток данных в источнике
   */
  static public boolean valueAndRest(CalcValue value, double requiredValue, CalcReader reader,
      String requiredReader) throws IOException {
    StringBuilder dataInReader = new StringBuilder();
    int c;
    while ((c = reader.read()) > 0) {
      dataInReader.append((char) c);
    }
    return (value.getValue() == requiredValue) && dataInReader.toString().equals(requiredReader);
  }
}
