package calc;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

public class CalcValueTest {
  static String[] doubleLiterals = {".2", ".2e3", ".2e-3", ".2e+3", "1", "1e3", "1e-3", "1e+3",
      "1.", "1.e3", "1.e-3", "1.e+3", "1.2", "1.2e3", "1.2e-3", "1.2e+3", "-.2", "-.2e3", "-.2e-3",
      "-.2e+3", "-1", "-1e3", "-1e-3", "-1e+3", "-1.", "-1.e3", "-1.e-3", "-1.e+3", "-1.2",
      "-1.2e3", "-1.2e-3", "-1.2e+3", "+.2", "+.2e3", "+.2e-3", "+.2e+3", "+1", "+1e3", "+1e-3",
      "+1e+3", "+1.", "+1.e3", "+1.e-3", "+1.e+3", "+1.2", "+1.2e3", "+1.2e-3", "+1.2e+3"};
  
  @Test
  public void testParse() throws IOException, ParseErrorException, TokenNotFoundException {
    for (String valueAsString : doubleLiterals) {
      CalcReader reader = new CalcReader(valueAsString);
      String assertMessage = String.format("Parsing %s", valueAsString);
      CalcValue value = CalcValue.parse(reader);
      double requiredValue = Double.parseDouble(valueAsString);
      assertTrue(assertMessage, Testutil.valueAndRest(value, requiredValue, reader, ""));
    }
  }

  @Test
  public void testParseStopAtSpace() throws IOException, ParseErrorException, TokenNotFoundException {
    // 1.2e3 45
    for (String valueAsString : doubleLiterals) {
      double requiredValue = Double.parseDouble(valueAsString);
      String valueWithTail = String.format("%s 45", valueAsString);
      CalcReader reader = new CalcReader(valueWithTail);
      CalcValue value = CalcValue.parse(reader);

      String assertMessage = String.format("Parsing %s", valueWithTail);
      assertTrue(assertMessage, Testutil.valueAndRest(value, requiredValue, reader, " 45"));
    }
  }

  @Test
  public void testParseStopAtAdd() throws IOException, ParseErrorException, TokenNotFoundException {
    for (String valueAsString : doubleLiterals) {
      double requiredValue = Double.parseDouble(valueAsString);
      String valueWithTail = String.format("%s+45", valueAsString);
      CalcReader reader = new CalcReader(valueWithTail);
      CalcValue value = CalcValue.parse(reader);

      String assertMessage = String.format("Parsing %s", valueWithTail);
      assertTrue(assertMessage, Testutil.valueAndRest(value, requiredValue, reader, "+45"));
    }
  }
  
  @Test
  public void testParseStopAtSubtract()
      throws IOException, ParseErrorException, TokenNotFoundException {
    for (String valueAsString : doubleLiterals) {
      double requiredValue = Double.parseDouble(valueAsString);
      String valueWithTail = String.format("%s-45", valueAsString);
      CalcReader reader = new CalcReader(valueWithTail);
      CalcValue value = CalcValue.parse(reader);

      String assertMessage = String.format("Parsing %s", valueWithTail);
      assertTrue(assertMessage, Testutil.valueAndRest(value, requiredValue, reader, "-45"));
    }
  }

  @Test
  public void testParseStopAtOther()
      throws IOException, ParseErrorException, TokenNotFoundException {
    for (String valueAsString : doubleLiterals) {
      double requiredValue = Double.parseDouble(valueAsString);
      String valueWithTail = String.format("%sq45", valueAsString);
      CalcReader reader = new CalcReader(valueWithTail);
      CalcValue value = CalcValue.parse(reader);

      String assertMessage = String.format("Parsing %s", valueWithTail);
      assertTrue(assertMessage, Testutil.valueAndRest(value, requiredValue, reader, "q45"));
    }
  }

  @Test
  public void testParseStopAtDot() throws IOException, ParseErrorException, TokenNotFoundException {
    // 1.2.45
    for (String valueAsString : doubleLiterals) {
      // Те литералы, в которых нет точки, пропускаем, тк, например
      // +1 даст нам 1.45, что распарсится целиком
      if (valueAsString.indexOf('.') < 0) {
        continue;
      }
      double requiredValue = Double.parseDouble(valueAsString);
      String valueWithTail = String.format("%s.45", valueAsString);
      CalcReader reader = new CalcReader(valueWithTail);
      CalcValue value = CalcValue.parse(reader);

      String assertMessage = String.format("Parsing %s", valueWithTail);
      assertTrue(assertMessage, Testutil.valueAndRest(value, requiredValue, reader, ".45"));
    }
  }

  @Test
  public void testParseStopAtE() throws IOException, ParseErrorException, TokenNotFoundException {
    // 1.2e3e45
    for (String valueAsString : doubleLiterals) {
      // Те литералы, в которых нет 'e', пропускаем, тк, например
      // +1.2 даст нам 1.2e45, что распарсится целиком
      if (valueAsString.indexOf('e') < 0) {
        continue;
      }
      double requiredValue = Double.parseDouble(valueAsString);
      String valueWithTail = String.format("%se45", valueAsString);
      CalcReader reader = new CalcReader(valueWithTail);
      CalcValue value = CalcValue.parse(reader);

      String assertMessage = String.format("Parsing %s", valueWithTail);
      assertTrue(assertMessage, Testutil.valueAndRest(value, requiredValue, reader, "e45"));
    }
  }

  @Test(expected=TokenNotFoundException.class)
  public void testParseStopAtPlusOperator()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = "+ 1";
    CalcReader reader = new CalcReader(valueAsString);
    CalcValue value = CalcValue.parse(reader); // execution will not pass this line
    fail(String.format("Parse error not catched got %f", value.getValue()));
  }
  
  @Test(expected=TokenNotFoundException.class)
  public void testParseStopAtPlusOperatorAndParentness()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = "+(1";
    CalcReader reader = new CalcReader(valueAsString);
    CalcValue value = CalcValue.parse(reader); // execution will not pass this line
    fail(String.format("Parse error not catched got %f", value.getValue()));
  }
  
  @Test(expected=TokenNotFoundException.class)
  public void testParseStopAtPlusAndGarbage()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = "+q1";
    CalcReader reader = new CalcReader(valueAsString);
    CalcValue value = CalcValue.parse(reader); // execution will not pass this line
    fail(String.format("Parse error not catched got %f", value.getValue()));
  }
  
  @Test(expected=TokenNotFoundException.class)
  public void testParseStopAtSubtractionOperator()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = "- 1";
    CalcReader reader = new CalcReader(valueAsString);
    CalcValue value = CalcValue.parse(reader); // execution will not pass this line
    fail(String.format("Parse error not catched got %f", value.getValue()));
  }
  
  @Test(expected=TokenNotFoundException.class)
  public void testParseStopAtSubtractionOperatorAndParentness()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = "-(1";
    CalcReader reader = new CalcReader(valueAsString);
    CalcValue value = CalcValue.parse(reader); // execution will not pass this line
    fail(String.format("Parse error not catched got %f", value.getValue()));
  }
  
  @Test(expected=TokenNotFoundException.class)
  public void testParseStopAtSubtractionAndGarbage()
      throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = "-q1";
    CalcReader reader = new CalcReader(valueAsString);
    CalcValue value = CalcValue.parse(reader); // execution will not pass this line
    fail(String.format("Parse error not catched got %f", value.getValue()));
  }
  
}
